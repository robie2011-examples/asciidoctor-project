#!/bin/bash
source ./env.sh

cd $DOC_FOLDER
rm -rf build/ 2>/dev/null
mkdir build

for D in `find $FOLDER_PREFIX* -type d -maxdepth 0`
do
  echo "render document $D"
  cd $D

  echo 'Building HTML...'
  asciidoctor -a toc=left -r asciidoctor-diagram -a data-uri -b xhtml5 index.adoc
  mv index.html ../build/$D.html

  echo ''
  echo ''
  echo ''

  cd ..
done
