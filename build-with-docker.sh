#!/bin/sh
source ./env.sh
docker run --rm -it -v$DOC_FOLDER:/data -w/data robie2011/asciidoctor:$IMAGE_VERSION bash build.sh