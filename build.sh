#!/bin/bash
SCRIPT_DIR="$(dirname $0)"

if [ "$SCRIPT_DIR" != "." ]; then
  echo 'Must be executed from its directory!'
  exit 1
fi

rm build/ 2>/dev/null
mkdir build

for D in `find doc-* -type d -maxdepth 0`
do
  echo "render document $D"
  cd $D

  echo 'Building HTML...'
  asciidoctor -a toc=left -r asciidoctor-diagram -a data-uri -b xhtml5 index.adoc
  mv index.html ../build/$D.html

  echo 'Building PDF...'
  asciidoctor -r asciidoctor-pdf -r asciidoctor-diagram -b pdf index.adoc
  mv index.pdf ../build/$D.pdf
  echo ''
  echo ''
  echo ''

  cd ..
done
