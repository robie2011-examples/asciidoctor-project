# Manuel Exports
Beispiele für Export:


PDF erzeugen:
```shell
# mit -r asciidoctor-diagram werden die benötigten Erweiterungen eingebunden
asciidoctor -b pdf -r asciidoctor-pdf -r asciidoctor-diagram sourcefile.ad
```

HTML erzeugen:
```shell
asciidoctor -b xhtml5 -r asciidoctor-diagram sourcefile.adoc
```
