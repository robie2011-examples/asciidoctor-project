# Gitlab CI Integration 
Siehe `.gitlab-ci.yml` im Repo.

Downloads 
  * https://gitlab.com/robie2011-examples/asciidoctor-project/-/jobs/artifacts/master/download?job=build_all
  * https://gitlab.com/robie2011-examples/asciidoctor-project/-/jobs/artifacts/master/raw/build/doc-example.html?job=build_all
  * Siehe auch https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html