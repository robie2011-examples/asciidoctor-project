# Installation

Installation:
```shell
gem install asciidoctor
```

PDF-Erweiterung (Pre-Release):
```shell
gem install asciidoctor-pdf --pre
```

Diagramm-Erweiterung:
```shell
gem install asciidoctor-diagram
```
Dazu wird zusätzlich `mermaid` benötigt:
```
npm install -g mermaid.cli
```