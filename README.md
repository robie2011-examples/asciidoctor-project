# ASCIIDoc Dokumentation

Dieses Repository enthält die Projektdokumentation.
Zur Erzeugung der Dokumente wird __ASCIIDoc__ Format verwendet. Eine erweiterte Variante davon ist [asciidoctor](https://asciidoctor.org/).

__Links__
* https://asciidoctor.org/docs/user-manual/
* [AsciiDoc Syntax Quick Reference](https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)


## Dokument Struktur
Die Build Skripte funktionieren nach folgender Annahme
 * Für jedes Dokument-Projekt wird ein eigenständiger Ornder auf Root-Ebene mit dem Präfix `doc-` erwartet
 * Für jedes Dokument-Projekt wird ein Startdokument mit dem Namen `index.adoc` erwartet.

## Export Skripte
Buildskripte
 * `build.html.sh`: HTML-Dokument für jedes Dokumentprojekt erstellen
 * `build.html-with-docker.sh`: HTML-Dokument für jedes Dokumentprojekt erstellen mit [vorgefertigten Docker Container](https://hub.docker.com/r/robie2011/asciidoctor/)
 * `build.sh`: HTML-Dokument und PDF-Dokument für jedes Dokumentprojekt erstellen
 * `build.sh`: HTML-Dokument und PDF-Dokument für jedes Dokumentprojekt erstellen  mit [vorgefertigten Docker Container](https://hub.docker.com/r/robie2011/asciidoctor/)

Die Ausgabe-Dateien von Buildskripten werden im Ordner `build/` gespeichert. 


## Vorgefertigter Container Umgebung
Eine vorgefertigte Container Umgebung kann wie folgt gestartet werden

```bash
docker run --rm -it -v$DOC_FOLDER:/data -w/data robie2011/asciidoctor:v18.12.18 bash
```

## Issues
* Nach Parts wird die Nummerierung der Chapter nicht zurückgesetzt. Workaround `:!chapter-number:`

## Tools Issue
* [Fix Ruby Warnings](https://github.com/prawnpdf/ttfunk/pull/41): Update `prawnpdf` ([How To](https://stackoverflow.com/questions/13626143/how-to-upgrade-rubygems))

