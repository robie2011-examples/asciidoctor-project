@echo off
REM execute from its directory

del /F /Q build\*.html && del /F /Q build\*.pdf && for /f "tokens=*" %%G in ('dir /b /a:d ".\doc-*"') do echo render %%G && cd %%G && asciidoctor -a toc=left -r asciidoctor-diagram -a data-uri -b xhtml5 index.adoc && move /Y index.html ..\build\%%G.html && asciidoctor -r asciidoctor-pdf -r asciidoctor-diagram -b pdf index.adoc && move /Y index.pdf ..\build\%%G.pdf && cd ..
